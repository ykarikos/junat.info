var jsdom = require('jsdom');
var http = require('http');
var Cache = require('expiring-lru-cache');
var utils = require('./../utils');
var rest = require('restler');
var uuid = require('node-uuid');
var moment = require('moment');
var stations = require('./stations');
var stationMap = require('./../stationMapping');

var cache = new Cache({size: 5000, expiry: 60*60*1000});

var tokenUrl = "http://shop.vr.fi/mobile/";
var postUrl = "http://shop.vr.fi/mobile/MobileSearch.do";
//var postUrl = "http://shop.vr.fi/onlineshop/SearchTimeTable.do";
var nextUrl = "http://shop.vr.fi/mobile/MobileJourneys_next.do";
var prevUrl = "http://shop.vr.fi/mobile/MobileJourneys_prev.do";
var token = "";

/*
http.get({host:'shop.vr.fi',port:80,path:'/mobile/',agent:false}, function(res) {
	var body = "";
	res.on('data', function (chunk) {
		body += chunk;
	});
	res.on('end', function () {
		console.log(body.match(/Mist[^:]+:/g)[0]);
	});
});
rest.get("http://shop.vr.fi/mobile/").on('complete', function(body) {
	console.log(body.match(/Mist[^:]+:/g)[0]);
});
*/

exports.find = function(request, apiResponse) {
	var params = request.body;
	if (params.token == undefined) {
		/*
		jsdom.env({
			html: tokenUrl,
			scripts: [
				'http://code.jquery.com/jquery-1.7.2.min.js'
			],
			done: function(errors, window) {
				var $ = window.$;
				utils.log("Fetched token %s", token);
				params.token = $('input[name="struts.token"]').val();
				doFind(request, apiResponse, params);
			}
		});
		*/
		params.token = uuid.v1();
		doFind(request, apiResponse, params);
	} else {
		doFind(request, apiResponse, params);
	}
};

exports.findNext = function(request, apiResponse) {
	var params = request.body;
	rest.get(nextUrl, {
		headers: {
			Cookie: cache.get(params.token),
			Referer: nextUrl
		}
	}).on('complete', function(data, response) {
			if (response.statusCode != 200) {
				utils.log("Invalid response %s from %s", response.statusCode, postUrl);
			}
			renderSuggestionResponse(params, data, apiResponse);
		});
}

exports.findPrevious = function(request, apiResponse) {
	var params = request.body;
	rest.get(prevUrl, {
		headers: {
			Cookie: cache.get(params.token),
			Referer: prevUrl
		}
	}).on('complete', function(data, response) {
			if (response.statusCode != 200) {
				utils.log("Invalid response %s from %s", response.statusCode, postUrl);
			}
			renderSuggestionResponse(params, data, apiResponse);
		});
}

function doFind(request, apiResponse, params) {
	if (params.nextDay) {
		params.date = moment(params.date, "DD.MM.YYYY").add('days', 1).format('DD.MM.YYYY');
		params.time = "00:00";
	}
	var searchData = {
		dateDay: params.date.substring(0,2),
		dateMonth: params.date.substring(3,5),
		dateYear: params.date.substring(6),
		from: stationMap.getStationCode(params.fromCity),
		"struts.token": params.token,
		"method:search": "Hae aikataulu",
		"struts.token.name": "struts.token",
		timeHours: params.time.substring(0,2),
		timeMinutes: params.time.substring(3,5),
		timeType: params.timeType, // 1=lähtö,2=tulo
		to: stationMap.getStationCode(params.toCity),
		//viewType: "mobile",
		headers: {
			Referer: "http://shop.vr.fi/mobile/"
		}
	};
	utils.log("Searching from %s to %s", searchData.from, searchData.to);
	rest.post(postUrl, {
		data: searchData
	}).on('complete', function(data, response) {
			if (response.statusCode != 200) {
				utils.log("Invalid response %s from %s", response.statusCode, postUrl);
			}
			cache.set(params.token, response.headers['set-cookie']);
			var html = data;
			renderSuggestionResponse(params, data, apiResponse);
		});
}

function renderSuggestionResponse(params, html, apiResponse) {
	utils.log('renderSuggestionResponse');
	jsdom.env(html, ['http://code.jquery.com/jquery-1.7.2.min.js'], function(errors, window) {
		try {
			var $ = window.$;
			var suggestions = [];
			if ($('.errorbox').length === 0) {
				$(".resultsep").each(function() {
					suggestions.push(parseSuggestion($, $(this)));
				});
			}
			// TODO: use stations.js & apiConnectionBetween for suggestions (if date is today)
			var json = {
				suggestions: suggestions,
				token: params.token,
				date: params.date,
				fromCity: params.fromCity,
				toCity: params.toCity
			};

			utils.log('fetching realtTimeInfo for %s', json);
			stations.realTimeInfo(json, apiResponse);

			//apiResponse.writeHead(200, { 'Content-Type': 'application/json' });
			//apiResponse.end(JSON.stringify(json), "utf8");
		} catch (err) {
			apiResponse.writeHead(500);
			apiResponse.end("error");
			throw err;
		}
	});
}

function parseSuggestion($, $result) {
	var suggestion = {connections: []};
	parseData($, $result, suggestion);
	$result.find('.trainchange').each(function() {
		parseData($, $(this), suggestion);
	});
	return suggestion;
}

function parseData($, $r, suggestion) {
	var data = $r.html().replace(/(\r\n|\n|\r)/gm,"");
	data = data.replace(/&nbsp;/gi,"");
	data = data.replace(/<(div)[^>]*>.*(<\/div>)/ig,"");
	data = data.replace(/<[^>]*>/gi,"");
	var times = data.match(/\d\d:\d\d/g);
	if (times && times.length == 2) {
		var cities = data.match(/[a-zäöå]+([\ -])*[a-zäöå]+/gi);
		var connection = {startTime:times[0], endTime:times[1]};
		connection.fromCity = cities[0];
		connection.toCity = cities[1];
		connection.train = parseTrain($, $r.html());
		suggestion.connections.push(connection);
	} else {
		data = data.replace(/( )|\t/gi,"");
		try {
			suggestion.changes = parseInt(data.match(/Vaihtoja(\d)/i)[0].replace(/[a-z]+/i,""));
		} catch (err) {
			suggestion.changes = 0;
		}
		suggestion.duration = data.match(/\d+:\d\d/)[0];
	}
}

function parseTrain($, data) {
	var train = data.substring(data.indexOf(","));
	train = train.replace(/&nbsp;/ig," ");
	train = train.replace(/<[^>]*>/gi,"");
	//console.log(train);
	return $.trim(train.match(/[a-zäöå0-9]+(\ )?[a-zäöå0-9]*/gi)[0]);
}

exports.updateToken = function() {
	jsdom.env({
		html: tokenUrl,
		scripts: [
			'http://code.jquery.com/jquery-1.7.2.min.js'
		],
		done: function(errors, window) {
			var $ = window.$;
			token = $('input[name="struts.token"]').val();
			utils.log("Updated token to %s", token);
		}
	});
}