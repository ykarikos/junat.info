var jsdom = require('jsdom');
var http = require('http');
var Cache = require('expiring-lru-cache');
var utils = require('./../utils');
var rest = require('restler');
var uuid = require('node-uuid');
var moment = require('moment');
var _ = require('underscore');
var xmlParser = require('libxml-to-js');
var stationMap = require('./../stationMapping');

var stationCache = new Cache({size: 100, expiry: 3*60*1000});

function realTimeInfo(suggestionsData, apiResponse) {
	console.log('calling realTimeInfo with date %s', suggestionsData.date);
	var expectedInfoReqs = calculateExpectedRequests(suggestionsData.suggestions);
	if (suggestionsData.date === moment().format("DD.MM.YYYY") && suggestionsData.suggestions.length > 0) {
		console.log('it is today');
		_.each(suggestionsData.suggestions, function(suggestion) {
			_.each(suggestion.connections, function(connection) {
				var fromCity = connection.fromCity;
				var fromTime = connection.startTime;

				var toCity = connection.toCity;
				var toTime = connection.endTime;

				stationInfo({
					callback: handleFromTrains,
					stationCode: getStationCode(fromCity),
					time: fromTime,
					fromCity: fromCity
				});

				stationInfo({
					callback: handleToTrains,
					stationCode: getStationCode(toCity),
					time: toTime,
					toCity: toCity
				});

				var fromTrains;
				var toTrains;
				function handleFromTrains(trains) {
					fromTrains = trains;
					handleResponse();
				}
				function handleToTrains(trains) {
					toTrains = trains;
					handleResponse();
				}
				function handleResponse() {
					if (fromTrains != undefined && toTrains != undefined) {
						expectedInfoReqs = expectedInfoReqs - 2;

						if (fromTrains.length === 1) {
							connection.realTimeInfo = fromTrains[0];
						} else if (fromTrains.length > 1) {
							var realResponse = [];
							_.each(fromTrains, function(fromTrain) {
								var found = _.find(toTrains, function(toTrain){
									return fromTrain.id === toTrain.id && fromTrain.scheduledDepartTime === fromTime && toTrain.scheduledDepartTime === toTime;
								});
								if (found) {
									realResponse.push(found);
								}
							});
							if (realResponse.length === 1) {
								connection.realTimeInfo = realResponse[0];
							}
						}

					}

					if (expectedInfoReqs === 0) {
						apiResponse.writeHead(200, { 'Content-Type': 'application/json' });
						apiResponse.end(JSON.stringify(suggestionsData), "utf8");
					}
				}

			});
		});
	} else {
		apiResponse.writeHead(200, { 'Content-Type': 'application/json' });
		apiResponse.end(JSON.stringify(suggestionsData), "utf8");
	}


	function calculateExpectedRequests(suggestions) {
		var number = 0;
		_.each(suggestions, function(suggestion) {
			number += suggestion.connections.length*2;
		});
		return number;
	}

}



function trainInfo(request, apiResponse) {
	var trainId = request.params.trainId;
	rest.get("http://188.117.35.14/TrainRSS/TrainService.svc/trainInfo?train=" + trainId).on('complete', function(data, response) {
		xmlParser(data, function (error, rss) {
			if (error) {
				throw 'Error parsing data';
			} else {
				var channel = rss.channel;
				var train = {
					id: channel.trainguid,
					name: (parseInt(channel.category) === 1 ? "Kaukojuna" : "Lähijuna") + " " + channel.title,
					heading: channel.heading,
					speed: channel.speed,
					from: channel.startStation,
					to: channel.endStation,
					fromCity: getStationCity(channel.startStation),
					toCity: getStationCity(channel.endStation),
					lateness: channel.lateness,
					stations: []
				};

				var coord = channel['georss:point'].split(' ');
				if (coord.length === 2) {
					var lat = parseFloat(coord[0]);
					var lon = parseFloat(coord[1]);
					if (lat > 0 && lon > 0) {
						train.coordinates = {
							lat: lat,
							lon: lon
						}
					}
				}

				var hasCompletedStations = false;
				var hasInProgressStations = false;
				_.each(getRssItems(rss), function(stationItem) {
					var station = {
						code: stationItem.title,
						name: getStationCity(stationItem.title),
						completed: parseInt(stationItem.completed) === 1 ? true : false,
						scheduledDepartTime: stationItem.scheduledDepartTime,
						etd: stationItem.etd,
						coordinates: getStationCoordinates(stationItem.title)
					}
					if (typeof station.scheduledDepartTime != 'string') {
						station.scheduledDepartTime = stationItem.scheduledTime;
						station.etd = stationItem.eta;
					}
					if (station.completed) {
						hasCompletedStations = true;
					} else {
						hasInProgressStations = true;
					}
					train.stations.push(station);
				});

				train.onRoute = hasCompletedStations && hasInProgressStations;
				var completed = false;
				if (train.onRoute) {
					var previousStation = "";
					for (var i = train.stations.length - 1; i >= 0; i--) {
						var station = train.stations[i];
						if (station.completed) {
							if (train.currentLeg == undefined) {
								train.currentLeg = {
									from: station.name,
									to: previousStation.name
								}
							}
							completed = true;
						}
						if (completed) {
							station.completed = true;
						}

						previousStation = station;
					}
				}

				apiResponse.writeHead(200, { 'Content-Type': 'application/json' });
				apiResponse.end(JSON.stringify(train), "utf8");

			}
		});

	});
}


function parseTrainFromRssItem(item) {
	try {
		var train = {
			id: item.guid['#'],
			name: (parseInt(item.category) === 1 ? "Kaukojuna" : "Lähijuna") + " " + item.title,
			//name: item.title,
			from: item.fromStation,
			fromCity: getStationCity(item.fromStation),
			to: item.toStation,
			toCity: getStationCity(item.toStation),
			type: parseInt(item.category) === 1 ? "Kaukojuna" : "Lähijuna",
			etd: item.etd,
			status: item.status,
			statusHuman: getTrainStatus(item.status),
			lateness: item.lateness,
			scheduledDepartTime: item.scheduledDepartTime,
			completed: parseInt(item.completed) === 1 ? true : false,
			reason: item.reasonCode
		}
		if (typeof train.scheduledDepartTime != 'string') {
			train.scheduledDepartTime = item.scheduledTime;
			train.etd = item.eta;
		}
		var point = item['georss:point'];
		if (point) {
			var coords = point.split(' ');
			if (coords.length === 2) {
				var lat = parseFloat(coords[0]);
				var lon = parseFloat(coords[1]);
				if (lat > 0 && long > 0) {
					train.coordinates = {
						lat: lat,
						lon: lon
					}
				}
			}

		}
		return train;
	} catch (err) {
		utils.log('ERROR parsing %s', item);
		return null;
	}
}

function getTrainStatus(status) {
	// (1=ajoissa, 2=myöhässä, 4=paljon myöhässä, 5=peruttu)
	var code = parseInt(status);
	switch (code) {
		case 1:
			return 'ajoissa';
		case 2:
			return 'myöhässä';
		case 4:
			return 'paljon myöhässä';
		case 5:
			return 'peruttu';
		default:
			return 'ei tiedossa';
	}
}

/**
 *
 * @param args: {callback: someFunction, stationCode: 'HKI', time:'14:15', toCity:'HKI' }
 * OR
 * @param args: {callback: someFunction, stationCode: 'HKI', time:'14:15', fromCity:'HKI' }
 */
function stationInfo(args) {
	args = args || {};

	var cachedTrains = stationCache.get(args.stationCode);
	if (cachedTrains) {
		doResponse(args, cachedTrains);
	} else {
		rest.get("http://188.117.35.14/TrainRSS/TrainService.svc/stationInfo?station=" + args.stationCode).on('complete', function(data, response) {
			xmlParser(data, function (error, result) {
				var trains = [];
				if (error) {
					console.error(error);
				} else {
					var items = getRssItems(result);
					var trains = _.map(items, function(item) {
						var train = parseTrainFromRssItem(item);
						if (train) {
							return train;
						}
					});
					if (trains && trains.length > 0) {
						stationCache.set(args.stationCode, trains);
					}
				}
				doResponse(args, trains);
			});
		});
	}

	function doResponse(args, trains) {
		var matchingTrains = [];
		_.each(trains, function(train) {
			if (train && train.scheduledDepartTime === args.time) {
				if (args.toCity && train.fromCity != args.toCity) {
					matchingTrains.push(train);
				} else if (args.fromCity && train.toCity != args.fromCity) {
					matchingTrains.push(train);
				}
			}
		});
		args.callback(matchingTrains);
	}
}


function getRssItems(result) {
	var itemsData = result.channel.item;
	return Array.isArray(itemsData) ? itemsData : [itemsData];
}

function getStationCode(city) {
	return stationMap.getStationCode(city);
}

function getStationCity(code) {
	return stationMap.getStationCity(code);
}

function getStationCoordinates(code) {
	return stationMap.getStationCoordinates(code);
}

//exports.apiStationTrains = apiStationTrains;
//exports.apiConnectionBetween = apiConnectionBetween;
exports.realTimeInfo = realTimeInfo;
exports.trainInfo = trainInfo;
