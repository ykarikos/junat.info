/*global jQuery:true, _:true, Backbone:true, Mustache:true */

(function(exports) {
	"use strict";
	var $ = jQuery;
	var cityTarget = "";

	var cities = ["Alavus","Dragsvik","Eläinpuisto-Zoo","Eno","Espoo","Haapajärvi","Haapamäki","Haarajoki","Hankasalmi","Hanko","Hanko-Pohjoinen","Harjavalta","Haukivuori","Heinävesi","Helsinki","Herrala","Hiekkaharju","Hikiä","Humppila","Huopalahti","Hyvinkää","Hämeenlinna","Höljäkkä","Iisalmi","Iittala","Ilmala","Imatra","Inkeroinen","Inkoo","Isokyrö","Joensuu","Jokela","Jorvas","Joutseno","Juupajoki","Jyväskylä","Jämsä","Järvelä","Järvenpää","Kajaani","Kannelmäki","Kannus","Karjaa","Karkku","Kauhava","Kauklahti","Kauniainen","Kausala","Kemi","Kemijärvi","Kera","Kerava","Kerimäki","Kesälahti","Keuruu","Kilo","Kirkkonummi","Kitee","Kiuruvesi","Kohtavaara","Koivuhovi","Koivukylä","Kokemäki","Kokkola","Kolari","Kolho","Kontiomäki","Koria","Korso","Kotka","Kotkan satama","Kouvola","Kuopio","Kupittaa","Kylänlahti","Kymi","Kyminlinna","Kyrölä","Käpylä","Lahti","Laihia","Lapinlahti","Lappeenranta","Lappila","Lappohja","Lapua","Lempäälä","Leppävaara","Lieksa","Lievestuore","Loimaa","Louhela","Luoma","Lusto","Malmi","Malminkartano","Mankki","Masala","Mikkeli","Misi","Mommila","Muhos","Muurola","Myllykoski","Myllymäki","Myyrmäki","Mäkkylä","Mäntsälä","Mäntyharju","Nastola","Nivala","Nokia","Nuppulinna","Nurmes","Oitti","Orivesi","Orivesi keskusta","Oulainen","Oulu","Oulunkylä","Paimenportti","Paltamo","Parikkala","Parkano","Parola","Pasila","Pello","Petäjävesi","Pieksämäki","Pietarsaari","Pihlajavesi","Pitäjänmäki","Pohjois-Haaga","Pori","Puistola","Pukinmäki","Punkaharju","Purola","Pyhäsalmi","Pännäinen","Rekola","Retretti","Riihimäki","Rovaniemi","Runni","Ruukki","Ryttylä","Salo","Santala","Saunakallio","Savio","Savonlinna","Savonlinna-Kauppatori","Seinäjoki","Siilinjärvi","Simpele","Siuntio","Skogby","Sukeva","Suonenjoki","Tammisaari","Tampere","Tapanila","Tavastila","Tervajoki","Tervola","Tikkurila","Toijala","Tolsa","Tornio","Tornio-Itäinen","Tuomarila","Turenki","Turku","Turku satama","Tuuri","Uimaharju","Utajärvi","Uusikylä","Vaala","Vaasa","Vainikkala","Valimo","Vammala","Vantaankoski","Varkaus","Vihanti","Viiala","Viinijärvi","Villähde","Vilppula","Vuonislahti","Ylistaro","Ylitornio","Ylivieska","Ähtäri"];

	$(function() {
		$.each(cities, function(i, city) {
			var $ul = $('#citySelectionList');
			var $li = $('<li></li>').appendTo($ul);
			var $a = $('<a href="#">'+city+'</a>').appendTo($li);
		});

		$('#citySelectionList').on('click', 'a', function() {
			var city = $(this).text();
			if (cityTarget === 'from') {
				setSelectedFromCity(city);
			} else {
				setSelectedToCity(city);
			}

			window.history.back();
		});

	});

	function showSelectCityDialog(url, options) {
		var city = url.hash.replace( /.*city=/, "" );
		var pageSelector = url.hash.replace( /\?.*$/, "" );
		var $page = $(pageSelector);

		var title, dataUrl;

		var $searchInput = $page.find('input[data-type="search"]');
		if ($searchInput.val() !== '') {
			$searchInput.val('').trigger('change');
		}

		if (city === 'from') {
			title = 'Valitse lähtöasema';
			dataUrl = '#valitse-lahtoasema';
		} else {
			title = 'Valitse kohdeasema';
			dataUrl = '#valitse-kohdeasema';
		}
		cityTarget = city;
		$('#citySelectionTitle').text(title);

		// We don't want the data-url of the page we just modified
		// to be the url that shows up in the browser's location field,
		// so set the dataUrl option to the URL for the category
		// we just loaded.
		//debugger;
		//options.dataUrl = url.href;
		options.dataUrl = dataUrl;


		// Now call changePage() and tell it to switch to
		// the page we just modified.
		$.mobile.changePage( $(pageSelector), options );
	}




	// http://jquerymobile.com/demos/1.1.0/docs/pages/page-dynamic.html

	function setSelectedFromCity(city) {
		$('#fromCity').data('selectedCity', city);
		$('#fromCity').find('.ui-btn-text').text(city);
	}

	function setSelectedToCity(city) {
		$('#toCity').data('selectedCity', city);
		$('#toCity').find('.ui-btn-text').text(city);
	}

	function getSelectedFromCity() {
		return $('#fromCity').data('selectedCity');
	}

	function getSelectedToCity() {
		return $('#toCity').data('selectedCity');
	}

	exports.showSelectCityDialog = showSelectCityDialog;
	exports.getSelectedFromCity = getSelectedFromCity;
	exports.getSelectedToCity = getSelectedToCity;

	exports.setSelectedToCity = setSelectedToCity;
	exports.setSelectedFromCity = setSelectedFromCity;


})(trainApp.citySelection = {});
