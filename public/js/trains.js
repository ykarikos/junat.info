/*global jQuery:true, _:true, Backbone:true, Mustache:true */

(function(exports) {
	"use strict";
	var $ = jQuery;
	var resultsShown = false;
	var searchHistoryShown = false;
	var trainDetailsShown = false;
	var MAX_STORAGE_CONNECTIONS = 20;
	var token;
	var searchData;
	var trainInfo;
	var googleMap;
	var trainLatLng;

	$(document).bind( "pagebeforechange", function( event, data ) {

		// We only want to handle changePage() calls where the caller is
		// asking us to load a page by URL.
		if ( typeof data.toPage === "string" ) {
			var url = $.mobile.path.parseUrl( data.toPage );
			if (isLinkTo(url,'#city_selection_dialog')) {
				trainApp.citySelection.showSelectCityDialog(url, data.options);
				event.preventDefault();
			} else if (isLinkTo(url, '#search_history_dialog')) {
				showSearchHistory(url, data.options);
				event.preventDefault();
			}
		}

		function isLinkTo(url, target) {
			return url.hash.indexOf(target) >= 0;
		}
	});

	$(function() {
		initConnections();
		$("#date").val(today());
		$("#time").val(timeNow());

		$("#searchConnections").click(function() {
			var data = {
				fromCity: trainApp.citySelection.getSelectedFromCity(),
				toCity: trainApp.citySelection.getSelectedToCity(),
				date: $('#date').val(),
				time: $("#time").val(),
				//timeType: parseInt($("#timeType").val())
				timeType: 1
			};

			if (!isValidData(data)) {
				alert (data.fromCity.length > 0 && data.fromCity == data.toCity ? "Hej, äijä! Mistä ja minne ovat samat kaupungit!?" : "Kamoon, anna kaikki tarvittavat tiedot, yo!");
				return;
			}
			$('#resultsTitle').text(data.fromCity + " → " + data.toCity + " (" + data.date + ")");
			addConnectionToStorage(data.fromCity, data.toCity);
			searchData = data;
			$.mobile.showPageLoadingMsg();
			$.post("/schedule", data)
				.success(function (response) {
					showSuggestions(response);
				}).error(function() {
					alert ("Mönkään meni... :(");
				}).complete(function() {
					$.mobile.hidePageLoadingMsg();
				});

			return false;
		});

		$('#next_connections').click(function() {
			findNextConnections();
			return false;
		});

		$('#previous_connections').click(function() {
			findPreviousConnections();
			return false;
		})

		$('[data-back="true"]').click(function() {
			window.history.back();
			return false;
		});


		$('#searchHistoryList').on('click', 'li a', function() {
			trainApp.citySelection.setSelectedFromCity($(this).data("from"));
			trainApp.citySelection.setSelectedToCity($(this).data("to"));

			history.back();
		});

		$('#resultsList').on('click', 'a[data-change-day]', function() {
			$.mobile.showPageLoadingMsg();
			$.post("/schedule", $.extend({nextDay: true}, searchData))
				.success(function (response) {
					searchData.date = response.date;
					showSuggestions(response);
				}).error(function() {
					alert ("Mönkään meni... :(");
				}).complete(function() {
					$.mobile.hidePageLoadingMsg();
				});
		});

		$('#resultsList').on('click', 'a[data-train-id]', loadTrainInfo);
		$('#trainDetailsList').on('click', '#refreshTrainDetails', loadTrainInfo);
		$('#trainDetailsList').on('click', '#showTrainDetailsMap', showMap);
		$('#kartta').on('click', '#refreshMap', refreshMap);
		$('#kartta').live('pageshow', function(event, ui) {
			resizeAndCenter();
			setTimeout(function() {
				resizeAndCenter();
			}, 400);

			function resizeAndCenter() {
				google.maps.event.trigger(googleMap, 'resize');
				centerMapIfOutsideBounds({forceCenter: true});
			}
		});

		function loadTrainInfo() {
			$.mobile.showPageLoadingMsg();
			$.get("/trainInfo/"+ $(this).attr('data-train-id'))
				.success(function (response) {
					showTrainDetails(response);
				}).error(function() {
					alert ("Mönkään meni... :(");
				}).complete(function() {
					$.mobile.hidePageLoadingMsg();
				});
		}

		function refreshMap() {
			google.maps.event.trigger(googleMap, 'resize');
			$.mobile.showPageLoadingMsg();
			$.get("/trainInfo/"+ trainInfo.id)
				.success(function (response) {
					trainInfo = response;
					showMap({refresh: true});
					centerMapIfOutsideBounds();
				}).error(function() {
					alert ("Mönkään meni... :(");
				}).complete(function() {
					$.mobile.hidePageLoadingMsg();
				});
		}
	});

	function initConnections() {
		var connections = getConnectionsFromStorage();
		if (connections.length > 0) {
			var c = connections[0];
			trainApp.citySelection.setSelectedFromCity(c.from);
			trainApp.citySelection.setSelectedToCity(c.to);
		}
	}

	function showSearchHistory(url, options) {
		options.dataUrl = '#hakuhistoria';

		var $ul = $('#searchHistoryList');
		$ul.html('');
		var connections = getConnectionsFromStorage();
		if (connections.length > 0) {
			$.each(connections, function(i, connection) {
				var $li = $('<li></li>').appendTo($ul);
				var $a = $('<a href="#">'+connection.from+' →<br/>'+connection.to+'</a>').appendTo($li);
				$a.data('from', connection.from);
				$a.data('to', connection.to);
			});
		} else {
			$ul.append('<li>Ei hakuja.</li>');
		}

		if (searchHistoryShown) {
			$ul.listview("refresh");
		}
		searchHistoryShown = true;
		$.mobile.changePage( $('#search_history_dialog'), options );
	}

	// https://developers.google.com/maps/documentation/javascript/tutorial#asynch
	function loadGoogleMapsScript() {
		var script = document.createElement("script");
		script.type = "text/javascript";
		script.src = "http://maps.googleapis.com/maps/api/js?key=AIzaSyC5OqXKdh0RSP0SVfpAldgRT26f3JQbVdw&sensor=true&callback=trainApp.trains.showMap";
		document.body.appendChild(script);
	}


	var markers = [];
	/*
	var stationIcon = new google.maps.MarkerImage(
		'/img/station.png',
		// This marker is 20 pixels wide by 32 pixels tall.
		new google.maps.Size(32, 37),
		// The origin for this image is 0,0.
		new google.maps.Point(0,0),
		// The anchor for this image is the base of the flagpole at 0,32.
		new google.maps.Point(32, 37)
	);
	*/
	var stationIcon = '/img/station.png';

	function showMap(args) {
		args = args || {};
		$.mobile.showPageLoadingMsg();
		if (typeof(google) == 'undefined' || typeof(google.maps) == 'undefined') {
			loadGoogleMapsScript();
		} else {
			var lat = 62.244269;
			var lon = 25.74646;
			if (trainInfo.coordinates) {
				lat = trainInfo.coordinates.lat;
				lon = trainInfo.coordinates.lon;
			}
			trainLatLng = new google.maps.LatLng(lat, lon);
			var mapOptions = {
				zoom: 11,
				center: trainLatLng,
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				streetViewControl: false,
				mapTypeControl: false,
				zoomControl: true,
				zoomControlOptions: {
					position: google.maps.ControlPosition.LEFT_TOP,
					style: google.maps.ZoomControlStyle.SMALL
				},
				panControl: true,
				panControlOptions: {
					position: google.maps.ControlPosition.RIGHT_TOP
				}
			}
			if (typeof googleMap == 'undefined') {
				googleMap = new google.maps.Map(document.getElementById("mapCanvas"), mapOptions);
			} else {
				deleteOverlays();
			}

			$.each(trainInfo.stations, function(i, station) {
				var coordinates = station.coordinates;
				if (coordinates) {
					// http://google-maps-utility-library-v3.googlecode.com/svn/tags/markerwithlabel/1.1.5/examples/basic.html
					addMarkerWithLabel({
						position: new google.maps.LatLng(coordinates.lat, coordinates.lon),
						title: station.name,
						icon: stationIcon,
						labelContent: station.name,
						labelAnchor: new google.maps.Point(22, 0),
						labelClass: "stationLabel"
						//labelStyle: {opacity: 0.75}
					});
				}
			});

			if (trainInfo.onRoute) {
				var heading = trainInfo.heading;
				var direction = "";
				if (isNaN(parseInt(heading)) === false) {
					if (heading > 348.75 || heading <= 11.25) {
						direction = "\u2191"; // North
					} else if (heading > 11.25 && heading <= 78.75) {
						direction = "\u2197"; // North-east
					} else if (heading > 78.75 && heading <= 101.25) {
						direction = "\u2192"; // East
					} else if (heading > 101.25 && heading <= 168.75) {
						direction = "\u2198"; // South-east
					} else if (heading > 168.75 && heading <= 191.25) {
						direction = "\u2193"; // South
					} else if (heading > 191.25 && heading <= 258.75) {
						direction = "\u2199"; // South-west
					} else if (heading > 258.75 && heading <= 281.25) {
						direction = "\u2190"; // West
					} else if (heading > 281.25 && heading <= 348.75) {
						direction = "\u2196"; // North-west
					}
				}

				var trainMarker = addMarkerWithLabel({
					position: trainLatLng,
					title: trainInfo.name,
					icon: '/img/train.png',
					//labelContent: direction + " " + trainInfo.name,
					labelContent: trainInfo.name,
					labelAnchor: new google.maps.Point(22, 0),
					labelClass: "trainLabel"
				});
			}


			$.mobile.hidePageLoadingMsg();
			$.mobile.changePage('#kartta');
		}

		function addMarkerWithLabel(args) {
			var marker = new MarkerWithLabel($.extend({
				map: googleMap
			}, args));
			markers.push(marker);
			return marker;
		}

		// Sets the map on all markers in the array.
		function setAllMap(map) {
			for (var i = 0; i < markers.length; i++) {
				markers[i].setMap(map);
			}
		}

		// Removes the overlays from the map, but keeps them in the array.
		function clearOverlays() {
			setAllMap(null);
		}

		// Deletes all markers in the array by removing references to them.
		function deleteOverlays() {
			clearOverlays();
			markers = [];
		}
	}

	function centerMapIfOutsideBounds(args) {
		args = args || {};
		if (googleMap && trainLatLng) {
			try {
				if (args.forceCenter === true || googleMap.getBounds().contains(trainLatLng) === false) {
					googleMap.setCenter(trainLatLng);
				}
			} catch (err) {

			}
		}
	}

	function showTrainDetails(train) {
		trainInfo = train;
		var $ul = $('#trainDetailsList');
		$ul.html('');

		$('#trainDetailsTitle').text(train.name);

		if (train.onRoute) {
			var $trainItem = $('<li data-theme="a"></li>').appendTo($ul);
			$trainItem.addClass('trainInfo');
			var $rightSide = $('<p class="ui-li-aside">Nopeus : <strong>'+train.speed+' km/h</strong></p>').appendTo($trainItem);
			$rightSide.append('<br/><a id="refreshTrainDetails" href="#" data-role="button" data-icon="refresh" data-inset="true" data-mini="true" class="ui-btn-right" data-train-id="'+train.id+'" data-iconpos="right">Päivitä</a>');
			if (train.coordinates) {
				$rightSide.append('<br/><a id="showTrainDetailsMap" href="#" data-role="button" data-icon="arrow-r" data-inset="true" data-mini="true" class="ui-btn-right" data-train-id="'+train.id+'" data-iconpos="right">Kartta</a>');
			}
			var $leftSide = $('<div class="leftSide"></div>').appendTo($trainItem);
			$leftSide.append('<p>Juna on juuri nyt välillä:</p>');
			$leftSide.append('<h4>'+train.currentLeg.from+' →<br/>' + train.currentLeg.to + '</h4>');
			$leftSide.append(getLatenessTextHtml(train.lateness));
		}

		$.each(train.stations, function(i, station) {
			var $li = $('<li data-theme="c"></li>').appendTo($ul);

			$li.append('<h3>'+station.name+'</h3>');
			if (train.onRoute && station.completed) {
				$li.addClass('trainCompleted');
			}
			if (station.scheduledDepartTime === station.etd) {
				$li.append('<p class="ui-li-aside"><strong>'+station.etd+'</strong></p>');
			} else {
				$li.append('<p class="ui-li-aside"><strong>'+station.etd+'</strong><br/><strike>'+station.scheduledDepartTime+'</strike></p>');
			}
		});


		if (trainDetailsShown) {
			$ul.listview("refresh");
			$('#refreshTrainDetails').button();
			$('#showTrainDetailsMap').button();
		}
		$.mobile.changePage('#junan-tiedot');
		trainDetailsShown = true;
	}

	function showSuggestions(response) {
		var $ul = $("#resultsList");
		token = response.token;
		$ul.html("");
		if (response.suggestions.length === 0) {
			if (parseInt(searchData.time.substring(0,2)) > 15) {
				$ul.append('<li><a href="#" data-change-day="next">Ei yhteyksiä. Vaihda seuraavaan päivään</a></li>');
			} else {
				$ul.append('<li>Ei hakutuloksia.</li>');
			}
		} else if (response.fromCity && response.toCity && response.date) {
			$('#resultsTitle').text(response.fromCity + " → " + response.toCity + " (" + response.date + ")");
		}
		$.each(response.suggestions, function(i, suggestion) {
			var changesText;
			switch(suggestion.changes)
			{
				case 0:
					changesText = "ei vaihtoja";
					break;
				case 1:
					changesText = "yksi vaihto";
					break;
				default:
					changesText = suggestion.changes + " vaihtoa";
			}
			$ul.append('<li data-role="list-divider">'+(i+1)+'. vaihtoehto: kesto '+suggestion.duration+', '+changesText+'</li>');
			var $li = $('<li></li>').appendTo($ul);
			var $table = $('<table></table>').appendTo($li);
			$.each(suggestion.connections, function(j, connection) {
				if (i == 0 && j == 0) {
					searchData.time = connection.startTime;
				}
				var $tr = $('<tr></tr>').appendTo($table);
				$tr.append('<td>'+connection.startTime+'</td>');
				$tr.append('<td>'+connection.fromCity+'</td>');
				$tr.append('<td>→</td>');
				$tr.append('<td>'+connection.endTime+'</td>');
				$tr.append('<td>'+connection.toCity+'</td>');
				$tr = $('<tr class="train"></tr>').appendTo($table);
				var text = connection.train;
				if (connection.realTimeInfo) {
					var lateness = connection.realTimeInfo.lateness;
					text = '<a href="#" class="ui-link" data-train-id="'+connection.realTimeInfo.id+'">'+connection.realTimeInfo.name+'</a>'+getLatenessTextHtml(lateness);
				}
				$tr.append('<td colspan="5">'+text+'</td>');
			});
		});
		// TODO: Fixme! http://forum.jquery.com/topic/dynamic-html-don-t-get-the-proper-classes-how-to-force-it
		if (resultsShown) {
			$ul.listview("refresh");
		}
		$.mobile.changePage($("#hakutulokset"), {transition: "fade", reloadPage: true});
		resultsShown = true;
	}

	function isValidData(data) {
		if (data.fromCity.length < 1 || data.toCity.length < 1) {
			return false;
		}
		if (data.date.match(/\d{2}\.\d{2}\.\d{4}/) == null) {
			return false;
		}
		if (data.time.match(/\d{2}/)) {
			data.time = data.time + ':00';
		}
		if (data.time.match(/\d{2}:\d{2}/) == null) {
			return false;
		}
		if (data.timeType != 1 && data.timeType != 2) {
			return false;
		}
		if (data.fromCity == data.toCity) {
			return false;
		}
		return true;
	}

	function getLatenessTextHtml(lateness) {
		if (parseInt(lateness) > 0) {
			return '<span class="late">(myöhässä ' + Math.ceil(lateness/60) + ' min)</span>';
		}
		return '<span class="late">&nbsp;</span>';
	}

	function today() {
		var d = new Date();
		var year, month, day;
		year = String(d.getFullYear());
		month = String(d.getMonth() + 1);
		if (month.length == 1) {
			month = "0" + month;
		}
		day = String(d.getDate());
		if (day.length == 1) {
			day = "0" + day;
		}
		return day + "." + month + "." + year;
	}

	function timeNow() {
		var d = new Date();
		var hours = String(d.getHours());
		if (hours.length == 1) {
			hours = "0" + hours;
		}
		var minutes = String(d.getMinutes());
		if (minutes.length == 1) {
			minutes = "0" + minutes;
		}
		return hours + ":" + minutes;
	}

	function findNextConnections() {
		$.mobile.showPageLoadingMsg();
		$.post("/scheduleNext", {token: token, date: searchData.date})
			.success(function (response) {
				showSuggestions(response);
			}).error(function() {
				alert ("Mönkään meni... :(");
			}).complete(function() {
				$.mobile.hidePageLoadingMsg();
			});
	}

	function findPreviousConnections() {
		$.mobile.showPageLoadingMsg();
		$.post("/schedulePrevious", {token: token, date: searchData.date})
			.success(function (response) {
				showSuggestions(response);
			}).error(function() {
				alert ("Mönkään meni... :(");
			}).complete(function() {
				$.mobile.hidePageLoadingMsg();
			});
	}

	function addConnectionToStorage(from, to) {
		var existingConnections = getConnectionsFromStorage();
		if (removePair(existingConnections, from, to)) {
			return;
		}
		var newData = "";
		newData += from + "@" + to;
		for (var i=0; i < existingConnections.length; i++) {
			if (i == (MAX_STORAGE_CONNECTIONS-1)) {
				break;
			}
			var conn = existingConnections[i];
			newData += "|" + conn.from + "@" + conn.to;
		}
		localStorage.setItem("connections", newData);
	}

	function removePair(connections, from, to) {
		for (var i=0; i < connections.length; i++) {
			var conn = connections[i];
			if (conn.from == from && conn.to == to) {
				connections.splice(i,1);
			}
		}
		return false;
	}

	function getConnectionsFromStorage() {
		var str = localStorage.getItem("connections");
		if (str) {
			var connectionPairs = str.split('|');
			var connections = [];
			for (var i=0; i < connectionPairs.length; i++) {
				var pair = connectionPairs[i].split("@");
				connections.push({from:pair[0],to:pair[1]});
			}
			return connections;
		}
		return [];
	}

	function calculateDistance() {
		// TODO : http://stackoverflow.com/questions/27928/how-do-i-calculate-distance-between-two-latitude-longitude-points
	}

	exports.getConnectionsFromStorage = getConnectionsFromStorage;
	exports.findNextConnections = findNextConnections;
	exports.showMap = showMap;
	exports.test = function() {
		//loadGoogleMapsScript();
		//$.mobile.changePage('#map');

		return trainLatLng;

	}
	exports.getGoogleMap = function() {
		return googleMap;
	};


})(trainApp.trains = {});
