var moment = require('moment');

exports.log = function() {
	arguments[0] = "["+moment().format()+"] " + arguments[0];
	console.log.apply(console, arguments);
}