
/**
 * Module dependencies.
 */

var express = require('express'),
	jsdom = require('jsdom'),
	schedule = require('./routes/schedule'),
	stations = require('./routes/stations'),
	stationMapping = require('./stationMapping'),
	utils = require('./utils'),
	hbs = require('hbs'),
	gzippo = require('gzippo'),
	bundleUp = require('bundle-up');
	routes = require('./routes');

var app = module.exports = express.createServer();

var mockedScheduleResponse = {"suggestions":[{"connections":[{"startTime":"01:47","endTime":"02:03","fromCity":"Helsinki","toCity":"Leppävaara","train":"Lähijuna","realTimeInfo":{"id":"H8405","name":"L","from":"HKI","fromCity":"Helsinki","to":"KKN","toCity":"Kirkkonummi","type":"Lähijuna","etd":"01:47","status":"1","statusHuman":"ajoissa","lateness":"0","scheduledDepartTime":"01:47","completed":false,"reason":{}}}],"changes":0,"duration":"0:16"},{"connections":[{"startTime":"05:19","endTime":"05:35","fromCity":"Helsinki","toCity":"Leppävaara","train":"Lähijuna"}],"changes":0,"duration":"0:16"}],"token":"ee8c4440-e5a5-11e1-8a0b-e15ff56327c0","date":"14.08.2012"};
var mockedTrainInfoResponse = {
	"id":"H8405",
	"name":"Lähijuna L",
	"heading":{},
	"speed":"0",
	"from":"HKI",
	"to":"KKN",
	"fromCity":"Helsinki",
	"toCity":"Kirkkonummi",
	"lateness":"0",
	"stations":[{"code":"HKI","name":"Helsinki","completed":false,"scheduledDepartTime":"01:47","etd":"01:47","coordinates":{"lat":60.1720917396682,"lon":24.9412457352685}},{"code":"PSL","name":"Pasila","completed":false,"scheduledDepartTime":"01:52","etd":"01:52","coordinates":{"lat":60.199054881001,"lon":24.9333345869716}},{"code":"ILA","name":"Ilmala","completed":false,"scheduledDepartTime":"01:54","etd":"01:54","coordinates":{"lat":60.2081567808029,"lon":24.9205839896604}},{"code":"HPL","name":"Huopalahti","completed":false,"scheduledDepartTime":"01:56","etd":"01:56","coordinates":{"lat":60.218365990445,"lon":24.8934628002039}},{"code":"VMO","name":"Valimo","completed":false,"scheduledDepartTime":"01:58","etd":"01:58","coordinates":{"lat":60.2221706066547,"lon":24.8758168247687}},{"code":"PJM","name":"Pitäjänmäki","completed":false,"scheduledDepartTime":"01:59","etd":"01:59","coordinates":{"lat":60.2233884103417,"lon":24.859771422591}},{"code":"MÄK","name":"Mäkkylä","completed":false,"scheduledDepartTime":"02:01","etd":"02:01","coordinates":{"lat":60.2213059911276,"lon":24.8429694862304}},{"code":"LPV","name":"Leppävaara","completed":false,"scheduledDepartTime":"02:03","etd":"02:03","coordinates":{"lat":60.2194802336929,"lon":24.8134577450556}},{"code":"KIL","name":"Kilo","completed":false,"scheduledDepartTime":"02:05","etd":"02:05","coordinates":{"lat":60.217935119038,"lon":24.7823960686559}},{"code":"KEA","name":"Kera","completed":false,"scheduledDepartTime":"02:07","etd":"02:07","coordinates":{"lat":60.2165702785059,"lon":24.7556295250905}},{"code":"KNI","name":"Kauniainen","completed":false,"scheduledDepartTime":"02:10","etd":"02:10","coordinates":{"lat":60.2119390946602,"lon":24.7308581218048}},{"code":"KVH","name":"Koivuhovi","completed":false,"scheduledDepartTime":"02:12","etd":"02:12","coordinates":{"lat":60.2071474132788,"lon":24.7012687033073}},{"code":"TRL","name":"Tuomarila","completed":false,"scheduledDepartTime":"02:14","etd":"02:14","coordinates":{"lat":60.206054027033,"lon":24.6817589572196}},{"code":"EPO","name":"Espoo","completed":false,"scheduledDepartTime":"02:16","etd":"02:16","coordinates":{"lat":60.2050697178862,"lon":24.6559798867302}},{"code":"KLH","name":"Kauklahti","completed":false,"scheduledDepartTime":"02:19","etd":"02:19","coordinates":{"lat":60.1894501477597,"lon":24.6003376990427}},{"code":"MNK","name":"Mankki","completed":false,"scheduledDepartTime":"02:20","etd":"02:20","coordinates":{"lat":60.1852574352316,"lon":24.5830801641194}},{"code":"LMA","name":"Luoma","completed":false,"scheduledDepartTime":"02:22","etd":"02:22","coordinates":{"lat":60.1731050401385,"lon":24.5508733692944}},{"code":"MAS","name":"Masala","completed":false,"scheduledDepartTime":"02:25","etd":"02:25","coordinates":{"lat":60.1586587763578,"lon":24.5391015403841}},{"code":"JRS","name":"Jorvas","completed":false,"scheduledDepartTime":"02:27","etd":"02:27","coordinates":{"lat":60.1378946392413,"lon":24.5127813696834}},{"code":"TOL","name":"Tolsa","completed":false,"scheduledDepartTime":"02:30","etd":"02:30","coordinates":{"lat":60.1176408999754,"lon":24.4702512523943}},{"code":"KKN","name":"Kirkkonummi","completed":false,"scheduledDepartTime":{},"etd":{},"coordinates":{"lat":60.1196405307238,"lon":24.4389860447007}}],
	"onRoute":false
};

process.on('uncaughtException', function (err) {
	utils.log('Caught exception: %s', err.stack);
});


var isProduction = app.settings.env == 'production';
var DEBUG = isProduction ? false : false;
bundleUp(app, __dirname + '/assets', {
	staticRoot: __dirname + '/public/',
	staticUrlRoot:'/',
	bundle: isProduction,
	minifyCss: isProduction,
	minifyJs: isProduction
});

// Configuration
app.configure(function(){
	app.set('views', __dirname + '/views');
	//app.set('view engine', 'jade');
	app.set('view engine', 'hbs');
	app.use(express.bodyParser());
	app.use(express.methodOverride());
	app.use(app.router);
	//Replace the default connect or express static provider with gzippo's
	//app.use(express.static(__dirname + '/public'));
	app.use(gzippo.staticGzip(__dirname + '/public'));
});

// HBS test stuff
(function() {
	hbs.registerHelper('link_to', function(context) {
		return "<a href='" + context.url + "'>" + context.body + "</a>";
	});
	hbs.registerHelper('link_to2', function(title, context) {
		return "<a href='/posts" + context.url + "'>" + title + "</a>"
	});
	hbs.registerHelper('list', function(items, fn) {
		var out = "<ul>";
		for(var i=0, l=items.length; i<l; i++) {
			out = out + "<li>" + fn(items[i]) + "</li>";
		}
		return out + "</ul>";
	});
	hbs.registerPartial('link2', '<a href="/people/{{id}}">{{name}}</a>');
})();


app.configure('development', function(){
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

app.configure('production', function(){
  app.use(express.errorHandler());
});

// Routes
app.get('/', function(req, res){
	res.render('index', {
		isProductionMode: isProduction,
		jsAssets: app._locals.renderJs(),
		cssAssets: app._locals.renderStyles(),
		appTitle: 'Junat.info',
		baseUrl: 'http://www.junat.info'
	});
});
app.post('/schedule', function(request, response) {
	if (DEBUG) {
		response.writeHead(200, { 'Content-Type': 'application/json; charset=utf-8' });
		response.end(JSON.stringify(mockedScheduleResponse), "utf8");
	} else {
		try {
			schedule.find(request, response);
		} catch (err) {
			utils.log('Caught exception: %s\nGiving error response...', err.stack);
			response.writeHead(500, { 'Content-Type': 'application/json' });
			response.end(JSON.stringify({}), "utf8");
		}
	}

});
app.post('/scheduleNext', function(request, response) {
	try {
		schedule.findNext(request, response);
	} catch (err) {
		utils.log('Caught exception: %s\nGiving error response...', err.stack);
		response.writeHead(500, { 'Content-Type': 'application/json' });
		response.end(JSON.stringify({}), "utf8");
	}
});
app.post('/schedulePrevious', function(request, response) {
	try {
		schedule.findPrevious(request, response);
	} catch (err) {
		utils.log('Caught exception: %s\nGiving error response...', err.stack);
		response.writeHead(500, { 'Content-Type': 'application/json' });
		response.end(JSON.stringify({}), "utf8");
	}
});
/*
app.get('/stationTrains/:city', function(request, response) {
	try {
		stations.apiStationTrains(request, response);
	} catch (err) {
		utils.log('Caught exception: %s\nGiving error response...', err.stack);
		response.writeHead(500, { 'Content-Type': 'application/json' });
		response.end(JSON.stringify({}), "utf8");
	}
});
*/
app.get('/trainInfo/:trainId', function(request, response) {
	if (DEBUG) {
		response.writeHead(200, { 'Content-Type': 'application/json; charset=utf-8' });
		response.end(JSON.stringify(mockedTrainInfoResponse), "utf8");
	} else {
		try {
			stations.trainInfo(request, response);
		} catch (err) {
			utils.log('Caught exception: %s\nGiving error response...', err.stack);
			response.writeHead(500, { 'Content-Type': 'application/json' });
			response.end(JSON.stringify({}), "utf8");
		}
	}
});

app.get('/admin/data', function(request, response) {
	try {
		stationMapping.adminData(request, response);
	} catch (err) {
		utils.log('Caught exception: %s\nGiving error response...', err.stack);
		response.writeHead(500, { 'Content-Type': 'application/json' });
		response.end(JSON.stringify({}), "utf8");
	}
});



/*
app.get('/connectionFromTo/:fromCity/:fromTime/:toCity/:toTime', function(request, response) {
	try {
		stations.apiConnectionBetween(request, response);
	} catch (err) {
		utils.log('Caught exception: %s\nGiving error response...', err.stack);
		response.writeHead(500, { 'Content-Type': 'application/json' });
		response.end(JSON.stringify({}), "utf8");
	}
});
*/

/*
app.get('/hbs', function(req, res){
	res.render('index', {
		title: 'Express Handlebars Test',
		// basic test
		name: 'Alan',
		hometown: "Somewhere, TX",
		kids: [{"name": "Jimmy", "age": "12"}, {"name": "Sally", "age": "4"}],
		// path test
		person: { "name": "Alan" }, company: {"name": "Rad, Inc." },
		// escapee test
		escapee: '<jail>escaped</jail>',
		// helper test
		posts: [{url: "/hello-world", body: "Hello World!"}],
		// helper with string
		posts2: [{url: "/hello-world", body: "Hello World!"}],
		// for block helper test
		people: [
			{firstName: "Yehuda", lastName: "Katz"},
			{firstName: "Carl", lastName: "Lerche"},
			{firstName: "Alan", lastName: "Johnson"}
		],
		people2: [
			{ name: { firstName: "Yehuda", lastName: "Katz" } },
			{ name: { firstName: "Carl", lastName: "Lerche" } },
			{ name: { firstName: "Alan", lastName: "Johnson" } }
		],
		// for partial test
		people3: [
			{ "name": "Alan", "id": 1 },
			{ "name": "Yehuda", "id": 2 }
		]
	});
});
*/

var port = process.env.PORT || 3000;
var intervalId;
app.listen(port, function() {
	console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);
	/*
	if (intervalId) {
		clearInterval(intervalId);
	}
	intervalId = setInterval(function() {
		schedule.updateToken();
	}, 5*60*1000);
	schedule.updateToken();
	*/
});
