// assets.js
module.exports = function(assets) {
	assets.root = __dirname;
	assets.addJs('/public/js/libs/jquery.min.js');
	assets.addJs('/public/js/libs/jquery.mobile-1.1.0.min.js');
	// start datebox
	assets.addJs('/public/js/libs/jqm-datebox.core.min.js');
	assets.addJs('/public/js/libs/jqm-datebox.mode.calbox.min.js');
	assets.addJs('/public/js/libs/jquery.mobile.datebox.i18n.fi.utf8.js');
	// end datebox
	assets.addJs('/public/js/libs/google-maps/markerwithlabel.js');
	//assets.addJs('/public/js/helper.js');
	assets.addJs('/public/js/localStorageImitation.js');
	assets.addJs('/public/js/trains.js');
	assets.addJs('/public/js/citySelection.js');


	// STYLES
	assets.addCss('/public/css/themes/trains.css');
	assets.addCss('/public/css/libs/jquery.mobile.structure-1.1.0.css');
	assets.addCss('/public/css/libs/jqm-datebox.css');
	assets.addCss('/public/css/custom.css');

}
